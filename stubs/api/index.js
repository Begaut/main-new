const { router: adminRouter, params } = require("./admin")

const router = require("express").Router();

// router.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "https://b2.inno-js.ru");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

router.get("/getMainData", (req, res) => {
  setTimeout(() => {

    if (params.videoSection.value) {

    }
    const data = require('./getMainData')
    res.send(data);
  }, 1000)
});

router.use('/admin', adminRouter)
module.exports = router;
