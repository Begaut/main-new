const router = require("express").Router();

const params = {
  videoSection: {
    value: false,
    type: 'checkbox'
  },
  success: {
    value: true,
    type: 'checkbox'
  }
}

router.get('/', (req, res) => {
  res.send(`
<html>
    <h1>hello from admin</h1>
    
    <form method="POST" action="/api/admin/submit">
        ${Object.keys(params).map(paramKey => {
          
          const param = params[paramKey];
          
          return `
            <label>
                ${paramKey}  
                <input name="${paramKey}" ${param.value ? 'checked' : ''} type="checkbox"></input>
            </label>
            <br/>
          `
        }).join('')}
       
        <button>Submit</button>
    </form>
</html>
  `)
})

router.post('/submit', (req, res) => {
  console.log(req);

  params

  // Change params values

  res.redirect('/api/admin')
})

module.exports = {
  router, params
};
