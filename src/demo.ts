
type Data =  {
    id: number,
    checked: boolean,
    children: Data[]
}

export const setChecked = (data: Data, id: number) => {
    if (data.id === id) {
        data.checked = true
    } else {
        data.children.forEach((childData) => {
            setChecked(childData, id)
        })
    }
}
