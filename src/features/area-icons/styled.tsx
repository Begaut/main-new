import styled from 'styled-components';

const calcResponsive = (min, max) =>
  `calc(${min}px + (${max} - ${min}) * ((100vw - 768px) / (1440 - 768)))`;

export const Wrapper = styled.section`
    height: 30px;
    align-self: center;
    justify-self: center;
    width: 100%;
    margin-bottom: ${calcResponsive(45, 83)};
    padding: 0 20px;
    display: flex;
    justify-content: center;

    > div {
        width: 100%;
        max-width: 1100px;
        margin: 0 40px;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }
`;

export const IconsBlock = styled.div`
    display: inline-flex;
    justify-content: space-around;
    width: 100%;
    min-width: 200px;

    &:first-child {
        margin-bottom: 15px;
    }

    @media (min-width: 506px) {
        width: 50%;

        &:first-child {
            margin-bottom: 0;
        }
    }

    > div {
        width: 43px;
        height: 30px;
        display: flex;
        justify-content: center;

        img {
            width: 24px;
            height: 30px;
        }
    }
`;
