const pkg = require('./package')
const path = require('path')
const webpackCopy = require("copy-webpack-plugin");

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        resolve: {
            alias: {
                '@main': path.resolve('src')
            }
        },
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        },
        plugins: [new webpackCopy({
            patterns: [
                { from: "./static", to: "static", noErrorOnMissing: true },
                { from: process.env.LOCALES, to: "locales", noErrorOnMissing: true }
            ],

        })]
    },
    navigations: {
        'undefined.main': '/undefined'
    },
    features: {
        'undefined': {
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        'teachers.api.base.url': '/api'
    }
}
